CREATE TABLE IF NOT EXISTS streets (id SERIAL PRIMARY KEY, name VARCHAR(50));

CREATE TABLE IF NOT EXISTS persons (id SERIAL PRIMARY KEY, first_name VARCHAR(50), last_name VARCHAR(50), age INT, id_street INT, FOREIGN KEY (id_street) REFERENCES streets(id));

INSERT INTO streets (name) VALUES ('Pushkina'), ('Shevchenko'), ('Chmelnitskogo'), ('Gagarina'), ('Mendeleeva'), ('Pravdi'), ('Gogola'), ('Minina');

INSERT INTO persons (first_name, last_name, age, id_street) VALUES ('Sergei', 'Kovaliov', 22, 5),
																   ('Nastya', 'Dolinskaya', 20, 5),
																   ('Anna', 'Kovaliova', 13, 4),
																   ('Alex', 'Demidenko', 20, 3),
																   ('Ehor', 'Hrudev', 21, 2),
																   ('Pavel', 'Kovaliov', 23, 1),
																   ('Katya', 'Dolinskaya', 19, 1),
																   ('Dima', 'Demidenko', 19, 2),
																   ('Grisha', 'Demidenko', 21, 2),
																   ('Sasha', 'Hrudev', 25, 3),
																   ('Jenya', 'Babich', 23, 1),
																   ('Georgy', 'Boridko', 19, 1),
																   ('Masha', 'Baranova', 19, 2),
																   ('Igor', 'Kubanov', 23, 1),
																   ('Andrey', 'Gorbunov', 23, ),
																   ('Olesya', 'Goncharova', 22, ),
																   ('Maksim', 'Lomovoy', 22, ),
																   ('Jenya', 'Vlasov', 20, 6),
                                                        		   ('Marina', 'Afanasieva', 16, 6),
													    		   ('Kristina', 'Golubeva', 19, 6),
													    		   ('Nikolai', 'Beregov', 17, 6);

SELECT COUNT(*) FROM persons; /* Общее количество жителей */

SELECT AVG(age) AS Average_age_of_persons FROM persons; /* Средний возраст жителей */

SELECT last_name FROM (SELECT last_name FROM persons ORDER BY last_name ASC) AS alphabet_order GROUP BY last_name; /* Отсортированный по алфавиту список фамилий без повторений */

SELECT last_name, COUNT(last_name) AS last_names_count FROM persons GROUP BY last_name ORDER BY last_names_count ASC; /* Список фамилий, с указанием количества повторений этих фамилий в общем списке */

SELECT last_name FROM persons WHERE last_name LIKE '%b%'; /* Фамилии, которые содержат в середине букву «б» */

SELECT first_name, last_name, age FROM persons WHERE id_street IS NULL; /* Список бездомных */

SELECT first_name, last_name, age FROM persons JOIN streets ON persons.id_street = streets.id WHERE age < 18 AND name = 'Pravdi'; /* Список несовершеннолетних, проживающих на проспекте Правды */

SELECT name, COUNT(id_street) AS lives_on_street FROM streets LEFT JOIN persons ON persons.id_street = streets.id GROUP BY name ORDER BY name; /* Упорядоченный по алфавиту список всех улиц с указанием, сколько жильцов живёт на улице */

SELECT name FROM streets WHERE CHAR_LENGTH(name) = 6; /* Список улиц, название которых состоит из 6-ти букв */

SELECT name, COUNT(id_street) AS lives_on_street FROM streets LEFT JOIN persons ON persons.id_street = streets.id GROUP BY name HAVING COUNT(id_street) < 3; /* Список улиц с количеством жильцов на них меньше 3 */

